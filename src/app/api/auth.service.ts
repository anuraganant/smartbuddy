import { Injectable } from '@angular/core';
import { UtilService } from './util.service';
import { ConstantService } from '../../services/constant.service';

@Injectable({
    providedIn: 'root'
})
export class AuthService {

    constructor(public util: UtilService, public cs: ConstantService) { }

    smartBuddyLogin(paramData, callback) {
        let url = this.cs.url.API_PATH + this.cs.url.LOGIN;
        paramData['action'] = 'login';
        var param = paramData;
        this.util.getRequest(url, param, function (data, status) {
            callback(data, status);
        }, function (err, status) {
            callback(err, status);
        });
    }

    smartBuddyLogout(paramData, callback) {
        let url = this.cs.url.API_PATH + this.cs.url.LOGIN;
        paramData['action'] = 'logout';
        var param = paramData;
        this.util.getRequest(url, param, function (data, status) {
            callback(data, status);
        }, function (err, status) {
            callback(err, status);
        });
    }

}
