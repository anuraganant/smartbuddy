import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { ToastController, Platform } from '@ionic/angular';
import { ConstantService } from '../../services/constant.service';
import { AlertUtils } from '../../providers/alertutils';
import { NgxSpinnerService } from 'ngx-spinner';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UtilService {
  JQUERY: any; LOADER = null; isApp = null; platformType = null;
  constructor(private spinner: NgxSpinnerService, public cs: ConstantService, private http: Http,
    public toastController: ToastController, public alertUtils: AlertUtils, public platform: Platform) {
    this.JQUERY = window["$"];
    this.LOADER = null;
    this.isApp = false;

    this.platformType = "";
    this.platform.ready().then(() => {
      if (this.platform.is('android') || this.platform.is('ios')) {
        this.isApp = true;
      } else {
        this.isApp = false;
      }
    });
  }

  getHeader() {
    var headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    return headers;
  };

  getRequest(url: string, param: any, successCallBack: any, errorCallback: any) {
    this.commonRequest("GET", url, param, successCallBack, errorCallback);
  };
  postRequest(url: string, param: any, successCallBack: any, errorCallback: any) {
    this.commonRequest("POST", url, param, successCallBack, errorCallback);
  };
  commonRequest(type: any, url: string, param: any, successCallBack: any, errorCallback: any) {
    let that = this;
    let headers = this.getHeader();
    let loaderHide = false;

    if (param["loaderHide"]) {
      delete param["loaderHide"];
      loaderHide = true;
    }

    let parameter = this.JQUERY.param(param);

    if (!loaderHide)
      this.spinner.show();

    if (loaderHide)
      this.spinner.hide();

    if (type == "GET") {
      this.http.get(url + '?' + parameter, {
        headers: headers
      })
        .pipe(
          map(res => res.json()) // or any other operator
        )
        .subscribe(
          data => {
            successCallBack(data.data, data.status, data);
            this.spinner.hide();
          },
          err => {
            if (!loaderHide) {
              this.spinner.hide();
              this.alertUtils.errorToast('Unable to process the request');
            }
            errorCallback(err, 0);
          }
        );
    } else if (type == "POST") {
      this.http.post(url, parameter, {
        headers: headers
      })
        .pipe(
          map(res => res.json()) // or any other operator
        )
        .subscribe(
          data => {
            successCallBack(data.data, data.status, data);
            this.spinner.hide();
          },
          err => {
            if (!loaderHide) {
              this.spinner.hide();
              this.alertUtils.errorToast('Unable to process the request');
            }
            errorCallback(err, 0);
          }
        );
    }
  }

  showSpinner() {
    this.spinner.show();
  }
  
  hideSpinner(){
    this.spinner.hide();
  }
}
