import { TestBed } from '@angular/core/testing';

import { SmartbuddyService } from './smartbuddy.service';

describe('SmartbuddyService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SmartbuddyService = TestBed.get(SmartbuddyService);
    expect(service).toBeTruthy();
  });
});
