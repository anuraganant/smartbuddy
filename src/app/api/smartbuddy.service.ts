import { Injectable } from '@angular/core';
import { UtilService } from './util.service';
import { ConstantService } from '../../services/constant.service';

@Injectable({
  providedIn: 'root'
})
export class SmartbuddyService {

  constructor(
    public util: UtilService,
    public cs: ConstantService,
  ) {

  }

  getLeads(paramData, callback) {
    let url = this.cs.url.API_PATH + this.cs.url.LOGIN, that = this;
    paramData['action'] = 'usrlead';
    var param = paramData;
    this.util.getRequest(url, param, function (data, status) {
      callback(data, status);
    }, function (err, status) {
      callback(err, status);
    });
  }

  getLivepop(paramData, callback) {
    let url = this.cs.url.API_PATH + this.cs.url.LOGIN, that = this;
    paramData['action'] = 'livePopup';
    var param = paramData;
    this.util.getRequest(url, param, function (data, status) {
      callback(data, status);
    }, function (err, status) {
      callback(err, status);
    });
  }

  updateLeads(paramData, callback) {
    let url = this.cs.url.API_PATH + this.cs.url.LOGIN, that = this;
    paramData['action'] = 'saveFollowUp';
    var param = paramData;
    this.util.getRequest(url, param, function (data, status) {
      callback(data, status);
    }, function (err, status) {
      callback(err, status);
    });
  }

  getSaleLeads(paramData, callback) {
    let url = this.cs.url.API_PATH + this.cs.url.LOGIN, that = this;
    paramData['action'] = 'gmLeadList';
    var param = paramData;
    this.util.getRequest(url, param, function (data, status) {
      callback(data, status);
    }, function (err, status) {
      callback(err, status);
    });
  }

  getUserList(paramData, callback) {
    let url = this.cs.url.API_PATH + this.cs.url.LOGIN, that = this;
    paramData['action'] = 'gmUsrList';
    var param = paramData;
    this.util.getRequest(url, param, function (data, status) {
      callback(data, status);
    }, function (err, status) {
      callback(err, status);
    });
  }

  assignLead(paramData, callback) {
    let url = this.cs.url.API_PATH + this.cs.url.LOGIN, that = this;
    paramData['action'] = 'leadAssign';
    var param = paramData;
    this.util.getRequest(url, param, function (data, status) {
      callback(data, status);
    }, function (err, status) {
      callback(err, status);
    });
  }
}
