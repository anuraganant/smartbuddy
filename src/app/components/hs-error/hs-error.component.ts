import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-hs-error',
  templateUrl: './hs-error.component.html',
  styleUrls: ['./hs-error.component.scss'],
})
export class HsErrorComponent implements OnInit {
  @Input('page-name') pageName: string;
  constructor() { }

  ngOnInit() {}

}
