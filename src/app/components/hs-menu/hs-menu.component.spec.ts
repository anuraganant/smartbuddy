import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HsMenuComponent } from './hs-menu.component';

describe('HsMenuComponent', () => {
  let component: HsMenuComponent;
  let fixture: ComponentFixture<HsMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HsMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HsMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
