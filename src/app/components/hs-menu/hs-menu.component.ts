import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { Router } from "@angular/router";

@Component({
  selector: 'app-hs-menu',
  templateUrl: './hs-menu.component.html',
  styleUrls: ['./hs-menu.component.scss']
})
export class HsMenuComponent implements OnInit {

  @Input('param-data') paramDetail: any[];
  @Output() menuClick = new EventEmitter<string>();

  constructor(public router: Router) {
    //console.log(this.paramDetail);
  }

  ngOnInit() {
  }

  getOptionClicked(menu) {
    //console.log(menu);
    if (menu.page == 'Share App' || menu.page == 'Logout') {
      this.menuClick.emit(menu.page);
    } else {
      this.router.navigate([menu.path]);
    }
  }
}