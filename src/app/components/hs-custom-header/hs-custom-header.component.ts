import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { NavController } from '@ionic/angular';
import { ActivatedRoute, Router } from "@angular/router";

@Component({
  selector: 'app-hs-custom-header',
  templateUrl: './hs-custom-header.component.html',
  styleUrls: ['./hs-custom-header.component.scss']
})
export class HsCustomHeaderComponent implements OnInit {

  @Input('param-title') paramTitle: any;
  @Output() iconClick = new EventEmitter<any>();

  constructor(
    public router:Router,
    public navCtrl: NavController,
  ) { }

  ngOnInit() {
    //console.log( this.paramTitle);
  }

  iconHandler(){
    //console.log('iconClick.emit');
    //this.iconClick.emit();
    this.navCtrl.navigateRoot(['/home']);
  }
}