import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HsCustomHeaderComponent } from './hs-custom-header.component';

describe('HsCustomHeaderComponent', () => {
  let component: HsCustomHeaderComponent;
  let fixture: ComponentFixture<HsCustomHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HsCustomHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HsCustomHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
