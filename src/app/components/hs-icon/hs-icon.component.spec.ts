import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HsIconComponent } from './hs-icon.component';

describe('HsIconComponent', () => {
  let component: HsIconComponent;
  let fixture: ComponentFixture<HsIconComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HsIconComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HsIconComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
