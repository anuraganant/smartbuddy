import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-hs-icon',
  templateUrl: './hs-icon.component.html',
  styleUrls: ['./hs-icon.component.scss']
})
export class HsIconComponent implements OnInit {

  @Input('color') iconcolor: string;
  @Input('size') fontSize: string;
  @Input('type') typeDetail: string;
  @Input('name') iconName: string;

  constructor() {

  }

  ngOnInit() {
    //console.log(this.iconcolor, this.fontSize, this.typeDetail, this.iconName);
  }

}
