import { Component, OnInit, Input } from '@angular/core';
import { Router } from "@angular/router";

@Component({
  selector: 'app-hs-footer',
  templateUrl: './hs-footer.component.html',
  styleUrls: ['./hs-footer.component.scss'],
})
export class HsFooterComponent implements OnInit {
  @Input('page-name') pageName: string;
  @Input('page-path') pagePath: string;
  constructor(
    public router: Router,
  ) { }

  ngOnInit() {
    //console.log(this.pagePath);
  }

  clickBtn(){
    this.router.navigate(['/donation']);
  }

}
