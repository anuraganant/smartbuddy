import { Component, OnInit, ViewChild } from '@angular/core';
import { AlertUtils } from '../../providers/alertutils';
import { JModel } from '../../services/JModel';
import { Platform, NavController, AlertController, ActionSheetController, MenuController, IonInfiniteScroll } from '@ionic/angular';
import { UserData } from '../../providers/userdata';
import { NgxSpinnerService } from 'ngx-spinner';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from "@angular/router";
import { SharedService } from '../../services/shared.service';
import { ConstantService } from '../../services/constant.service';
import { LoginForm } from '../../services/form-constants.model';
import { AuthService } from '../api/auth.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  loginDetail: LoginForm = {
    mobile: '',
    password: '',
  }

  splashImage = true; userName: any; flag = 0;

  constructor(
    public sharedService: SharedService,
    public navController:NavController,
    public authService: AuthService,
    public route: ActivatedRoute,
    public router: Router,
    public spinner: NgxSpinnerService,
    public menuCtrl: MenuController,
    public alertUtils: AlertUtils,
    public jModel: JModel,
    public userData: UserData,
    public alertCtrl: AlertController,
    public platform: Platform,
    public constantService: ConstantService,
    public actionSheetController: ActionSheetController
  ) {
    this.getConfigData();
  }

  ngOnInit() {

  }

  getConfigData() {
    let that = this;
    this.setUserName(function (response, status) {
      //console.log(response, status);
      that.splashImage = false;
      if (status == 1) {
        that.jModel.set('userData', response);
        that.navController.navigateRoot(['/features', response]);
        that.userData.login(that.userName.value, response, function () { });
        that.userData = that.jModel.get('userData');
      } else {
        that.flag = 1;
      }
    });
  }


  setUserName(callBack: any) {
    let that = this;
    this.userData.getUserDetails(function (d: any) {
      //console.log("ReLogin", d);
      if (d && d["password"] && d["password"] != "" && d["mobile_no"] && d["mobile_no"] != "" && d["mobile_no"].length == 10) {
        that.userName = d.user_name;
        that.submitRelogin(d["mobile_no"], d["password"], function (data, status) {
          //console.log(data, status)
          if (status == 0) {
            that.alertUtils.errorToast(that.constantService.message.server);
          } else if (status == 1) {
            that.userData.setUserDetails(data);
            that.jModel.set('userData', data);
            that.jModel.set('Marker',{value:1});
          }
          callBack(data, status);
        })
      } else {
        callBack(0);
      }
    });
  }

  submitRelogin(mobile: any, password: any, callBack: any): void {
    let param = {
      mobile: mobile,
      password: password,
    }
    this.authService.smartBuddyLogin(param, function (data, status) {
      callBack(data, status);
    });
  }

  submitLogin(f: NgForm, values) {
    if (!f.valid) {
      this.alertUtils.errorToast(this.constantService.message.mandatory);
      return;
    };
    let that = this;
    let param = values;
    this.userName = param['mobile'];

    this.authService.smartBuddyLogin(param, function (data, status) {
      if (status == 0) {
        that.alertUtils.errorToast(that.constantService.message.loginfail);
      } else if (status == 1) {
        that.jModel.set('userData', data);
        that.jModel.set('Marker',{value:0});
        //that.alertUtils.successToast(that.constantService.message.login);
        that.userData.login(that.userName.value, data, function () {
          that.navController.navigateRoot(['/features', data]);
        });
      }
    });
  }

  ionViewDidLeave() {
    window['$']('#defaultLoading').hide();
    window['$']('#homeLoading').hide();
  }
}
