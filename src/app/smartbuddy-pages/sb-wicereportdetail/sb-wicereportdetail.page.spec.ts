import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SbWicereportdetailPage } from './sb-wicereportdetail.page';

describe('SbWicereportdetailPage', () => {
  let component: SbWicereportdetailPage;
  let fixture: ComponentFixture<SbWicereportdetailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SbWicereportdetailPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SbWicereportdetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
