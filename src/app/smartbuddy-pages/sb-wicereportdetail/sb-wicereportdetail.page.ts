import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { TimeUtils } from '../../../providers/timeutils';
import { AlertUtils } from '../../../providers/alertutils';
import { Downloader, DownloadRequest } from '@ionic-native/downloader/ngx';
import { DocumentViewer, DocumentViewerOptions } from '@ionic-native/document-viewer/ngx';
import { UtilService } from '../../api/util.service';
import { FileOpener } from '@ionic-native/file-opener/ngx';

@Component({
  selector: 'app-sb-wicereportdetail',
  templateUrl: './sb-wicereportdetail.page.html',
  styleUrls: ['./sb-wicereportdetail.page.scss'],
})
export class SbWicereportdetailPage implements OnInit {
  paramData: any = {}; leadDetails: any = [];
  constructor(
    public activatedRoute: ActivatedRoute,
    public timeUtils: TimeUtils,
    public downloader: Downloader,
    public alertUtils: AlertUtils,
    public documentViewer: DocumentViewer,
    public utilService: UtilService,
    public fileOpener: FileOpener,
  ) { }

  ngOnInit() {
    let that = this;
    this.activatedRoute.params.subscribe(params => {
      this.paramData = params;
      //console.log(this.paramData);
      this.leadDetails.push(
        { title: 'Branch Name', value: this.paramData.hartrodt_branch },
        { title: 'Company Name', value: this.paramData.company_name },
        { title: 'Company Address', value: this.paramData.comp_address1 + ', ' + this.paramData.comp_address2 + ', ' + this.paramData.comp_city + ', ' + this.paramData.comp_state },
        { title: 'Company Phone', value: this.paramData.comp_phone },
        { title: 'Contact Person Name', value: this.paramData.contact_person },
        { title: 'Contact Person Phone', value: this.paramData.contact_person_no },
        { title: 'Contact Person Email', value: this.paramData.contact_person_email },
        { title: 'Followup Date', value: (this.paramData.next_followup_date != '' && this.paramData.next_followup_date != "null" && this.paramData.next_followup_date != '0000-00-00') ? this.paramData.next_followup_date : 'N/A' },
        { title: 'Followup Time', value: (this.paramData.next_followup_time != '' && this.paramData.next_followup_time != "null") ? this.timeUtils.formatAMPM(this.paramData.next_followup_time.split(":")[0], this.paramData.next_followup_time.split(":")[1]) : 'N/A' },
        { title: 'Comment', value: (this.paramData.comment != '' && this.paramData.comment != "null") ? this.paramData.comment : 'N/A' },
        { title: 'Discussion', value: (this.paramData.discussion != '' && this.paramData.discussion != "null") ? this.paramData.discussion : 'N/A' },
        { title: 'New Contact Person Name', value: (this.paramData.new_contact_name != '' && this.paramData.new_contact_name != "null") ? this.paramData.new_contact_name : 'N/A' },
        { title: 'New Contact Person Phone', value: (this.paramData.new_contact_no != '' && this.paramData.new_contact_no != "null") ? this.paramData.new_contact_no : 'N/A' },
        { title: 'Recording', value: (this.paramData.recording != '' && this.paramData.recording != null) ? this.paramData.recording : 'N/A' },
        { title: 'Lead Type', value: this.paramData.lead_type == 2 ? 'International' : 'Domestic' },
      );
      //console.log(this.leadDetails);
    });
  }

  downloadAttachment(path) {
    let pathName = this.paramData.company_name + ' Report';
    this.alertUtils.alertToast("Downloading please wait!!");
    var request: DownloadRequest = {
      uri: path,
      title: pathName,
      description: '',
      mimeType: '',
      visibleInDownloadsUi: false,
      notificationVisibility: 0,
      destinationInExternalPublicDir: {
        dirType: 'SmartBuddy',
        subPath: path.split("/").pop(),
      }
    };

    this.downloader.download(request)
      .then((location: string) => {
        //this.alertUtils.alertToast("Successfully download !! Path: SmartBuddy/" + path.split("/").pop());
        this.openFile(location, pathName);
      })
      .catch((error: any) => {
        this.alertUtils.errorToast(error);
      });
  }


  openFile(location, pathName) {

    this.fileOpener.open(location, 'application/pdf')
      .then(() => {
        //this.alertUtils.alertToast('File is opened')
      })
      .catch(e => {
        this.alertUtils.alertToast('Error opening file, ' + e)
      });

    // const options: DocumentViewerOptions = {
    //   title: pathName
    // }
    // this.documentViewer.viewDocument(location, 'application/pdf', options)
  }
}
