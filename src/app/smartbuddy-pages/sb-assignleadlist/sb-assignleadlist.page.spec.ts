import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SbAssignleadlistPage } from './sb-assignleadlist.page';

describe('SbAssignleadlistPage', () => {
  let component: SbAssignleadlistPage;
  let fixture: ComponentFixture<SbAssignleadlistPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SbAssignleadlistPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SbAssignleadlistPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
