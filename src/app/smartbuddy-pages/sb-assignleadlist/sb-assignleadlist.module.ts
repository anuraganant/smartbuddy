import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';
import { SharedModule } from '../../../app/shared.module';
import { SbAssignleadlistPage } from './sb-assignleadlist.page';

const routes: Routes = [
  {
    path: '',
    component: SbAssignleadlistPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [SbAssignleadlistPage]
})
export class SbAssignleadlistPageModule {}
