import { Component, OnInit, ViewChild } from '@angular/core';
import { SmartbuddyService } from '../../api/smartbuddy.service';
import { ActivatedRoute, Router } from "@angular/router";
import { IonInfiniteScroll, AlertController, ActionSheetController } from '@ionic/angular';
import { AlertUtils } from '../../../providers/alertutils';

@Component({
  selector: 'app-sb-assignleadlist',
  templateUrl: './sb-assignleadlist.page.html',
  styleUrls: ['./sb-assignleadlist.page.scss'],
})
export class SbAssignleadlistPage implements OnInit {
  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;
  paramData: any = {}; userList: any = []; leadList: any = []; itemLength = null; activeLead: any;

  constructor(
    public activatedRoute: ActivatedRoute,
    public alertUtils: AlertUtils,
    public alertController: AlertController,
    public router: Router,
    public smartbuddyService: SmartbuddyService,
    public actionSheetController: ActionSheetController
  ) { }

  ngOnInit() {
    let that = this;
    this.activatedRoute.params.subscribe(params => {
      that.paramData = params;
      //console.log(that.paramData);
      that.getLeadList();
      that.getUserList();
    });
  }

  assignedLead(lead) {
    this.activeLead = lead;
    let status = false;
    let data = this.userList, tmp = [];
    for (let i in data) {
      tmp.push({
        name: data[i].user_name,
        type: 'radio',
        label: data[i].id == this.activeLead.sellerId ? data[i].user_name + " - Assigned" : data[i].user_name,
        value: data[i].id,
        checked: data[i].id == this.activeLead.sellerId ? true : false
      });
      if (data[i].id == this.activeLead.sellerId) {
        status = true;
      }
      //console.log(status)
    }
    this.presentActionSheet(tmp, status);
  }

  async presentActionSheet(tmp, status) {
    //console.log(tmp, status);
    const actionSheet = await this.actionSheetController.create({
      header: status == true ? 'Lead already assigned to someone. Do you want to continue?' : 'Do you want to continue?',
      buttons: [{
        text: 'Continue',
        icon: 'done-all',
        cssClass: 'successCss',
        handler: () => {
          //console.log('Delete clicked');
          this.presentAlertCheckbox(tmp);
        }
      }, {
        text: 'Cancel',
        icon: 'close',
        role: 'cancel',
        cssClass: 'dangerCss',
        handler: () => {
          //console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
  }

  async presentAlertCheckbox(tmp) {
    const alert = await this.alertController.create({
      header: 'Select User for ' + this.activeLead.company_name,
      inputs: tmp,
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'alertCancel',
          handler: () => {
            //console.log('Confirm Cancel');
          }
        }, {
          text: 'Ok',
          cssClass: 'alertConfirm',
          handler: (data) => {
            //console.log('Confirm Ok', data , this.activeLead.id);
            this.submitAssignedLead(data, this.activeLead.id)
          }
        }
      ],
      backdropDismiss: true
    });

    await alert.present();
  }

  submitAssignedLead(userID, leadID) {
    let that = this;
    this.leadList = [];
    let param = {
      userId: userID,
      leadId: leadID,
      status: 2,
      loaderHide: true
    }

    this.smartbuddyService.assignLead(param, function (response, status) {
      if (status == 1) {
        that.alertUtils.successToast("Lead Assigned Successfully");
        that.getLeadList();
      } else {
        that.alertUtils.errorToast('Unable to process request.');
      }
    });
  }

  getUserList() {
    let that = this;
    this.leadList = [];
    let param = {
      userId: this.paramData.id,
      userType: 2
    }

    this.smartbuddyService.getUserList(param, function (response, status) {
      if (status == 1) {
        that.userList = response;
      } else {
        that.userList = [];
      }
    });
  }

  getLeadList() {
    let that = this;
    this.leadList = [];
    let param = {
      userId: this.paramData.id,
      userType: 2
    }

    this.smartbuddyService.getSaleLeads(param, function (response, status) {
      if (status == 1) {
        let data = response;
        setTimeout(function () {
          for (let i in data) {
            if (data[i]['sellerId'] != '' || data[i]['sellerId'] != null) {
              for (let y in that.userList) {
                if (data[i]['sellerId'] == that.userList[y]['id']) {
                  data[i]['sellerName'] = that.userList[y]['user_name'];
                }
              }
            }
            that.leadList = data;
            that.itemLength = that.leadList.length;
          }
        }, 500);
      } else {
        that.leadList = [];
        that.itemLength = that.leadList.length;
      }
    });
  }

  doInfinite(infiniteScroll) {
    let that = this;
    let start = this.itemLength;

    let param = {
      userId: this.paramData.id,
      userType: 2,
      start: start,
      loaderHide: true
    };

    this.smartbuddyService.getSaleLeads(param, function (response, status) {
      if (response.length > 0) {
        for (let i in response) {
          setTimeout(function () {
            for (let i in response) {
              if (response[i]['sellerId'] != '' || response[i]['sellerId'] != null) {
                for (let y in that.userList) {
                  if (response[i]['sellerId'] == that.userList[y]['id']) {
                    response[i]['sellerName'] = that.userList[y]['user_name'];
                  }
                }
              }
              that.leadList.push(response[i]);
              that.itemLength = that.leadList.length;
            }
          }, 500);
        }
        infiniteScroll.target.complete();
      } else {
        infiniteScroll.target.complete();
        infiniteScroll.disabled = true;
      }
    });
  }
}
