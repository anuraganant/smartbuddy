import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SbFeaturesPage } from './sb-features.page';

describe('SbFeaturesPage', () => {
  let component: SbFeaturesPage;
  let fixture: ComponentFixture<SbFeaturesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SbFeaturesPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SbFeaturesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
