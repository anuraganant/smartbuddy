import { Component, OnInit, NgZone } from '@angular/core';
import { AlertUtils } from '../../../providers/alertutils';
import { NavController, AlertController } from '@ionic/angular';
import { ActivatedRoute, Router } from "@angular/router";
import { UserData } from '../../../providers/userdata';
import { AuthService } from '../../api/auth.service';
import { ConstantService } from '../../../services/constant.service';

@Component({
  selector: 'app-sb-features',
  templateUrl: './sb-features.page.html',
  styleUrls: ['./sb-features.page.scss'],
})
export class SbFeaturesPage implements OnInit {
  paramData: any = {};
  constructor(
    public router: Router,
    public authService: AuthService,
    public alertCtrl: AlertController,
    public constantService: ConstantService,
    public activatedRoute: ActivatedRoute,
    public alertUtils: AlertUtils,
    public userData: UserData,
    public navCtrl: NavController,
    public ngZone:NgZone,
  ) {

  }

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
      this.paramData = params;
      //console.log(this.paramData);
    });
  }

  openClickedButton(label) {
    //console.log(label);
    if (label == 'FollowUp' || label == 'Report' || label == 'LeadsDomestic' || label == 'LeadsOverseas') {
      let data = JSON.stringify(this.paramData)
      this.router.navigate(['/wicereportlist', { data: data, type: label }]);
    } else if (label == 'Combined' || label == 'LivePopups') {
      let data = JSON.stringify(this.paramData)
      this.router.navigate(['/livepopup', { data: data, type: label }]);
    } else if (label == 'Assignlead') {
      this.router.navigate(['/assignleadlist', this.paramData]);
    }
  }

  async logoutAlert() {
    let that = this;
    let confirm = await this.alertCtrl.create({
      header: 'Alert!',
      message: 'Do you really want to logout?',
      buttons: [
        {
          text: 'Cancel',
          cssClass: 'alertCancel',
          handler: () => {

          }
        },
        {
          text: 'Ok',
          cssClass: 'alertConfirm',
          handler: () => {
            that.callLogoutApi();
          }
        }
      ],
      backdropDismiss: true
    });
    await confirm.present();
  }

  callLogoutApi() {
    let that = this;
    let param = {}

    this.authService.smartBuddyLogout(param, function (data, status) {
      if (status == 0) {
        that.alertUtils.errorToast(that.constantService.message.server);
      } else if (status == 1) {
        that.userData.logout(function () {
          that.ngZone.run(() => { that.navCtrl.navigateRoot(['/home']) });
        });
      }
    });
  }
}
