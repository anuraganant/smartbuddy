import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SbLivepopupPage } from './sb-livepopup.page';

describe('SbLivepopupPage', () => {
  let component: SbLivepopupPage;
  let fixture: ComponentFixture<SbLivepopupPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SbLivepopupPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SbLivepopupPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
