import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { SmartbuddyService } from '../../api/smartbuddy.service';
import { ActivatedRoute, Router } from "@angular/router";
import { WiceForm } from '../../../services/form-constants.model';
import { AlertUtils } from '../../../providers/alertutils';
import { TimeUtils } from '../../../providers/timeutils';
import { ConstantService } from '../../../services/constant.service';
import { UserData } from '../../../providers/userdata';
import { JModel } from '../../../services/JModel';
import { NgForm } from '@angular/forms';
import { formatDate } from '@angular/common';
import { NativeGeocoder, NativeGeocoderResult, NativeGeocoderOptions } from '@ionic-native/native-geocoder/ngx';
import { ActionSheetController } from '@ionic/angular';
import { SpeechRecognition } from '@ionic-native/speech-recognition/ngx'; import { Downloader, DownloadRequest } from '@ionic-native/downloader/ngx';
import { DocumentViewer, DocumentViewerOptions } from '@ionic-native/document-viewer/ngx';
import { UtilService } from '../../api/util.service';
import { FileOpener } from '@ionic-native/file-opener/ngx';
declare var google: any;

@Component({
  selector: 'app-sb-livepopup',
  templateUrl: './sb-livepopup.page.html',
  styleUrls: ['./sb-livepopup.page.scss'],
})
export class SbLivepopupPage implements OnInit {
  @ViewChild('map') mapElement: ElementRef;
  map: any; paramData: any = {}; mapLoading = null; leadList: any = []; activeForm = false; activedId = null;
  flag = 1; todayDate: any; time: any; userId = null; leadId = null; icon = ''; leadDetails = [];
  nameFlag = 0; noFlag = 0; activeTable = false; contentFlag = null; title = ''; leadType = '';
  taskList: any = []; itemLength = null; recordedText = ''; selRepData: any;

  wiceDetail: WiceForm = {
    contact_person: '',
    contact_person_no: '',
    newContactName: '',
    newContactNo: '',
    company_name: '',
    comp_phone: '',
    comp_city: '',
    comp_state: '',
    revelant: true,
    changeName: false,
    changeNo: false,
    comment: '',
    discussion: '',
    nextFollowupDate: '',
    nextFollowupTime: '',
  }

  constructor(
    public smartbuddyService: SmartbuddyService,
    public actionSheetController: ActionSheetController,
    public speechRecognition: SpeechRecognition,
    public jModel: JModel,
    public router: Router,
    public userData: UserData,
    public nativeGeocoder: NativeGeocoder,
    public timeUtils: TimeUtils,
    public constantService: ConstantService,
    public activatedRoute: ActivatedRoute,
    public downloader: Downloader,
    public alertUtils: AlertUtils,
    public documentViewer: DocumentViewer,
    public utilService: UtilService,
    public fileOpener: FileOpener,
  ) {
    let dt = new Date();
    dt.setDate(dt.getDate() + 2);
    this.todayDate = formatDate(dt, 'yyyy-MM-dd', 'en');
    //console.log(this.todayDate);
    let time = '10:00';
    let newtime = time.split(':');
    this.time = this.timeUtils.getformatTime(newtime[0], newtime[1]);
    //console.log(this.time);
  }

  ngOnInit() {
    let that = this;
    this.activatedRoute.params.subscribe(params => {
      this.leadType = params.type
      this.paramData = JSON.parse(params.data);
      //console.log(this.leadType, this.paramData);
      if (this.leadType == 'LivePopups') {
        this.contentFlag = 0;
        this.fetchLeads();
        //this.loadMaps();
      } else {
        this.contentFlag = 1;
        this.getTaskList();
      }
    });
    this.userData.getUserDetails(function (data) {
      that.userId = data.id;
    });
  }

  getTaskList() {
    let that = this;
    this.taskList = [];
    let param = {
      id: this.paramData.id
    }

    this.smartbuddyService.getLeads(param, function (response, status) {
      //console.log(response);
      if (status == 1) {
        let tmpArr = [];
        for (let i in response) {
          if (response[i]['lead_type'] == 1 || response[i]['lead_type'] == 2) {
            tmpArr.push(response[i]);
          }
        }
        that.taskList = tmpArr;
      } else {
        that.taskList = [];
      }
      //console.log(that.taskList);
      that.itemLength = that.taskList.length;
    });
  }

  doInfinite(infiniteScroll) {
    let that = this;
    let start = this.itemLength;

    let param = {
      id: this.paramData.id,
      start: start,
      loaderHide: true
    };

    this.smartbuddyService.getLeads(param, function (response, status) {
      if (response.length > 0) {
        for (let i in response) {
          if (response[i]['lead_type'] == 1 || response[i]['lead_type'] == 2) {
            that.taskList.push(response[i]);
          }
        }
        that.itemLength = that.taskList.length;
        infiniteScroll.target.complete();
      } else {
        infiniteScroll.target.complete();
        infiniteScroll.disabled = true;
      }
    });
  }

  openClickedButton(item) {
    this.presentActionSheet("", item);
  }

  fetchLeads() {
    let that = this;
    this.leadList = [];
    let options: NativeGeocoderOptions = {
      useLocale: true,
      maxResults: 2
    };

    let param = {
      id: this.paramData.id
    }
    this.smartbuddyService.getLivepop(param, function (response, status) {
      if (status == 1) {
        for (let i in response) {
          if (response[i]['lead_type'] == 1 || response[i]['lead_type'] == 2 || response[i]['leadStatus'] == 2) {
            //console.log((response[i]));
            let add = response[i]['comp_address1'];
            let city = response[i]['comp_city'];
            let state = response[i]['comp_state'];
            let place = add + ',' + city + ',' + state;
            //alert(place);
            that.nativeGeocoder.forwardGeocode(place, options)
              .then((coordinates: NativeGeocoderResult[]) => {
                //alert('The coordinates are latitude=' + coordinates[0].latitude + ' and longitude=' + coordinates[0].longitude);
                response[i]['title'] = place;
                response[i]['lat'] = coordinates[0].latitude;
                response[i]['long'] = coordinates[0].longitude;
                response[i]['type'] = (response[i]['lead_type'] == 1) ? 'domestic' : (response[i]['lead_type'] == 2) ? 'international' : '';
                that.loadMap(response);
              })
              .catch((error: any) => {
                that.alertUtils.errorToast(error);
              });
          }
        }
      }
    });
  }

  loadMap(json) {
    let that = this;
    this.mapLoading = null;
    setTimeout(function () {
      let defaultlatLng = new google.maps.LatLng(json[0].lat, json[0].long);

      let mapOptions = {
        center: defaultlatLng,
        zoom: 3,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      }

      that.map = new google.maps.Map(that.mapElement.nativeElement, mapOptions);
      for (var i = 0, length = json.length; i < length; i++) {
        let data = json[i];
        let latLng = new google.maps.LatLng(data.lat, data.long);
        that.addMarker(data, latLng);
      }
      that.mapLoading = 1;
    }, 1000);
  }

  loadMaps() {
    let that = this;
    this.leadList = [];
    let param = {
      id: this.paramData.id
    }

    let options: NativeGeocoderOptions = {
      useLocale: true,
      maxResults: 2
    };

    this.smartbuddyService.getLivepop(param, function (response, status) {
      if (status == 1) {
        for (let i in response) {
          if (response[i]['lead_type'] == 1 || response[i]['lead_type'] == 2 || response[i]['leadStatus'] == 2) {
            response[i]['lat'] = 22.43543534;
            response[i]['long'] = 75.5435435;
            response[i]['type'] = (response[i]['lead_type'] == 1) ? 'domestic' : (response[i]['lead_type'] == 2) ? 'international' : ''
            that.leadList.push(response[i]);
          }
        }
      } else {
        that.leadList = [];
      }
      //console.log(that.leadList);
    });

    this.mapLoading = null;
    setTimeout(function () {
      let defaultlatLng = new google.maps.LatLng(28.6998822, 77.2549402);

      let mapOptions = {
        center: defaultlatLng,
        zoom: 4,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      }

      that.map = new google.maps.Map(that.mapElement.nativeElement, mapOptions);
      for (var i = 0, length = that.leadList.length; i < length; i++) {
        let data = that.leadList[1];
        let latLng = new google.maps.LatLng(data.lat, data.long);
        that.addMarker(data, latLng);
      }
      that.mapLoading = 1;
    }, 1000);
  }

  addMarker(data, latLng) {
    var icons = {
      domestic: {
        icon: 'assets/images/markerdom.png'
      },
      international: {
        icon: 'assets/images/markerint.png'
      }
    };
    let marker = new google.maps.Marker({
      map: this.map,
      animation: google.maps.Animation.DROP,
      position: latLng,
      icon: icons[data.type].icon,
    });

    let content = data;
    this.addInfoWindow(marker, content);
  }

  addInfoWindow(marker, content) {
    let that = this;
    var contentString = content.comp_city;

    let infoWindow = new google.maps.InfoWindow({
      content: contentString
    });
    infoWindow.open(this.map, marker);

    google.maps.event.addListener(marker, 'click', () => {
      infoWindow.close(that.map, marker);
      that.jModel.set('Marker', { value: 1 });
      that.presentActionSheet(infoWindow, content);
    });
  }


  async presentActionSheet(infoWindow, content) {
    this.selRepData = {};
    this.leadDetails = [];
    const actionSheet = await this.actionSheetController.create({
      header: 'Select option to proceed',
      buttons: [{
        text: 'View Lead',
        icon: 'clipboard',
        cssClass: 'successCss',
        handler: () => {
          this.openTable(infoWindow, content);
        }
      }, {
        text: 'Report',
        icon: 'checkbox-outline',
        cssClass: 'successCss',
        handler: () => {
          this.openForm(infoWindow, content);
        }
      }, {
        text: 'Cancel',
        icon: 'close',
        role: 'cancel',
        cssClass: 'dangerCss',
        handler: () => {
          //console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
  }

  openTable(infoWindow, content) {
    //console.log(infoWindow);
    this.activedId = content.id;
    this.selRepData = content;
    //console.log(this.selRepData);
    this.title = content.company_name + ' ' + 'Report';
    this.leadDetails.push(
      { title: 'Branch Name', value: content.hartrodt_branch },
      { title: 'Company Name', value: content.company_name },
      { title: 'Company Address', value: content.comp_address1 + ', ' + content.comp_address2 + ', ' + content.comp_city + ', ' + content.comp_state },
      { title: 'Company Phone', value: content.comp_phone },
      { title: 'Contact Person Name', value: content.contact_person },
      { title: 'Contact Person Phone', value: content.contact_person_no },
      { title: 'Contact Person Email', value: content.contact_person_email },
      { title: 'Followup Date', value: (content.next_followup_date != '' && content.next_followup_date != null && content.next_followup_date != '0000-00-00') ? content.next_followup_date : 'N/A' },
      { title: 'Followup Time', value: (content.next_followup_time != '' && content.next_followup_time != null) ? this.timeUtils.formatAMPM(content.next_followup_time.split(":")[0], content.next_followup_time.split(":")[1]) : 'N/A' },
      { title: 'Comment', value: (content.comment != '' && content.comment != null) ? content.comment : 'N/A' },
      { title: 'Discussion', value: (content.discussion != '' && content.discussion != null) ? content.discussion : 'N/A' },
      { title: 'New Contact Person Name', value: (content.new_contact_name != '' && content.new_contact_name != null) ? content.new_contact_name : 'N/A' },
      { title: 'New Contact Person Phone', value: (content.new_contact_no != '' && content.new_contact_no != null) ? content.new_contact_no : 'N/A' },
      { title: 'Recording', value: (content.recording != '' && content.recording != null) ? content.recording : 'N/A' },
      { title: 'Lead Type', value: content.lead_type == 2 ? 'International' : 'Domestic' },
    )
    //console.log(this.leadDetails);
    if (infoWindow != '') {
      infoWindow.close();
    }
    this.contentFlag = 2;
    this.activeTable = true;
  }

  downloadAttachment(path) {
    let pathName = this.paramData.company_name + ' Report';
    this.alertUtils.alertToast("Downloading please wait!!");
    var request: DownloadRequest = {
      uri: path,
      title: pathName,
      description: '',
      mimeType: '',
      visibleInDownloadsUi: false,
      notificationVisibility: 0,
      destinationInExternalPublicDir: {
        dirType: 'SmartBuddy',
        subPath: path.split("/").pop(),
      }
    };

    this.downloader.download(request)
      .then((location: string) => {
        //this.alertUtils.alertToast("Successfully download !! Path: SmartBuddy/" + path.split("/").pop());
        this.openFile(location, pathName);
      })
      .catch((error: any) => {
        this.alertUtils.errorToast(error);
      });
  }


  openFile(location, pathName) {

    this.fileOpener.open(location, 'application/pdf')
      .then(() => {
        //this.alertUtils.alertToast('File is opened')
      })
      .catch(e => {
        this.alertUtils.alertToast('Error opening file, ' + e)
      });

    // const options: DocumentViewerOptions = {
    //   title: pathName
    // }
    // this.documentViewer.viewDocument(location, 'application/pdf', options)
  }

  openForm(infoWindow, content) {
    //console.log(content);
    // alert(JSON.stringify(content))
    this.title = 'Update Lead';
    if (infoWindow != '') {
      infoWindow.close();
    }
    this.contentFlag = 2;
    this.activeForm = true;
    this.leadId = content.id;
    this.recordedText = content.recording;
    this.recordedText = content.recording != '' && content.recording != null ? content.recording : 'No Voice Record';
    this.wiceDetail = {
      contact_person: content.contact_person,
      contact_person_no: content.contact_person_no,
      newContactName: content.new_contact_name != null ? content.new_contact_name : '',
      newContactNo: content.new_contact_no != null ? content.new_contact_no : '',
      company_name: content.company_name,
      comp_phone: content.comp_phone,
      comp_city: content.comp_city,
      comp_state: content.comp_state,
      revelant: true,
      changeName: false,
      changeNo: false,
      comment: content.comment != '' && content.comment != null ? content.comment : '',
      discussion: content.discussion != '' && content.discussion != null ? content.discussion : '',
      nextFollowupDate: content.next_followup_date != '' && content.next_followup_date != null && content.next_followup_date != '0000-00-00' ? content.next_followup_date : this.todayDate,
      nextFollowupTime: content.next_followup_time != '' && content.next_followup_time != null ? content.next_followup_time : this.time,
    }
    //console.log(this.wiceDetail.nextFollowupDate);
    //this.router.navigate(['/updatelead', this.leadList[0]]);
  }

  revelantToggle(value) {
    //console.log(value);
    if (!value) {
      this.flag = 0;
      this.wiceDetail.discussion = '';
      this.wiceDetail.nextFollowupDate = this.todayDate;
      this.wiceDetail.nextFollowupTime = this.time;
    } else {
      this.flag = 1;
      this.wiceDetail.comment = '';
    }
  }

  changeNameToggle(value) {
    //console.log(value);
    if (!value) {
      this.nameFlag = 0;
    } else {
      this.nameFlag = 1;
    }
  }

  changeNoToggle(value) {
    //console.log(value);
    if (!value) {
      this.noFlag = 0;
    } else {
      this.noFlag = 1;
    }
  }

  getVoiceText() {
    let that = this;
    let options = {
      language: 'en-US',
      matches: 3,
      prompt: '',
      showPopup: true,
      showPartial: false
    }

    this.speechRecognition.hasPermission()
      .then((hasPermission: boolean) => {
        if (hasPermission) {
          this.speechRecognition.startListening(options)
            .subscribe(
              (matches: string[]) => {
                if (matches[0] != '') {
                  let string = matches[0];
                  this.recordedText = string;
                } else {
                  this.alertUtils.errorToast("Please Try Again!!");
                }
              },
              (onerror) => {
                this.alertUtils.errorToast("Please Try Again!!");
              }
            )
        } else {
          this.speechRecognition.requestPermission()
            .then(
              () => {
                this.speechRecognition.startListening(options)
                  .subscribe(
                    (matches: string[]) => {
                      if (matches[0] != '') {
                        let string = matches[0];
                        this.recordedText = string;
                      } else {
                        this.alertUtils.errorToast("Please Try Again!!");
                      }
                    },
                    (onerror) => {
                      this.alertUtils.errorToast("Please Try Again!!");
                    }
                  )
              },
              () => {
                this.alertUtils.errorToast('Access Denied');
              }
            )
        }
      })

    // Request permissions

  }

  async showRecordedText() {
    const actionSheet = await this.actionSheetController.create({
      header: 'Message:' + this.recordedText,
      buttons: [{
        text: 'Cancel',
        cssClass: 'dangerCss',
        role: 'cancel',
        handler: () => {
          //console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
  }

  submitForm(f: NgForm, values) {
    //console.log(values);
    let that = this;
    if (!f.valid) {
      this.alertUtils.errorToast(this.constantService.message.mandatory);
      return;
    };
    let param = {};
    if (this.flag == 0) {
      param["company_name"] = values["company_name"];
      param["comp_phone"] = values["comp_phone"];
      param["newContactName"] = values["newContactName"];
      param["newContactNo"] = values["newContactNo"];
      param["comp_city"] = values["comp_city"];
      param["comp_state"] = values["comp_state"];
      param["comment"] = values["comment"];
      param["status"] = 1;
      param["leadId"] = this.leadId;
      param["userId"] = this.userId;
      param["recording"] = values["recording"];
    } else {
      param["company_name"] = values["company_name"];
      param["comp_phone"] = values["comp_phone"];
      param["newContactName"] = values["newContactName"];
      param["newContactNo"] = values["newContactNo"];
      param["comp_city"] = values["comp_city"];
      param["comp_state"] = values["comp_state"];
      param["discussion"] = values["discussion"];
      param["status"] = 2;
      param["leadId"] = this.leadId;
      param["userId"] = this.userId;
      param["recording"] = this.recordedText;

      if (values['nextFollowupTime'] && values['nextFollowupTime'] != '' && values['nextFollowupTime']['hour']) {
        let followuptime1 = values["nextFollowupTime"]['hour']['text'];
        let followuptime2 = values["nextFollowupTime"]['minute']['text'];
        param["nextFollowTime"] = followuptime1 + ":" + followuptime2;
      } else {
        param["nextFollowTime"] = values["nextFollowupTime"];
      }
      param['nextFollowupDate'] = this.timeUtils.dayDiff(values['nextFollowupDate'], 0);
      param['nextFollowDay'] = this.timeUtils.getDayName(values['nextFollowupDate']);

    }

    //console.log(JSON.stringify(param));

    this.smartbuddyService.updateLeads(param, function (response, status) {
      if (status == 1) {
        that.cancelModal();
        f.resetForm();
        that.alertUtils.successToast(that.constantService.message.update);
      } else {
        that.alertUtils.errorToast(that.constantService.message.server);
      }
      //console.log(that.leadList);
    });
  }

  cancelDetails() {
    this.activeTable = false;
    this.activeForm = false;
    this.recordedText = '';
    if (this.leadType == 'LivePopups') {
      this.contentFlag = 0;
      this.fetchLeads();
      //this.loadMaps();
    } else {
      this.contentFlag = 1;
      this.getTaskList();
    }
  }

  cancelModal() {
    if (this.leadType == 'LivePopups') {
      this.contentFlag = 0;
      this.fetchLeads();
      //this.loadMaps();
    } else {
      this.contentFlag = 1;
      this.getTaskList();
    }
    this.title = '';
    this.recordedText = '';
    if (this.activeForm) {
      this.activeForm = false;
      this.nameFlag = 0;
      this.noFlag = 0;
      this.wiceDetail = {
        contact_person: '',
        contact_person_no: '',
        newContactName: '',
        newContactNo: '',
        company_name: '',
        comp_phone: '',
        comp_city: '',
        comp_state: '',
        revelant: true,
        changeName: false,
        changeNo: false,
        comment: '',
        discussion: '',
        nextFollowupDate: '',
        nextFollowupTime: '',
      };
    } else if (this.activeTable) {
      this.activeTable = false;
      this.leadDetails = [];
    }
  }
}
