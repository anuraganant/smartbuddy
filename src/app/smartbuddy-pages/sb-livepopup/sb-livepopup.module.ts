import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';
import { SharedModule } from '../../../app/shared.module';
import { SbLivepopupPage } from './sb-livepopup.page';

const routes: Routes = [
  {
    path: '',
    component: SbLivepopupPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [SbLivepopupPage]
})
export class SbLivepopupPageModule {}
