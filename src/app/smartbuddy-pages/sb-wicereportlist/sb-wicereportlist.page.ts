import { Component, OnInit, ViewChild } from '@angular/core';
import { SmartbuddyService } from '../../api/smartbuddy.service';
import { ActivatedRoute, Router } from "@angular/router";
import { IonInfiniteScroll } from '@ionic/angular';

@Component({
  selector: 'app-sb-wicereportlist',
  templateUrl: './sb-wicereportlist.page.html',
  styleUrls: ['./sb-wicereportlist.page.scss'],
})
export class SbWicereportlistPage implements OnInit {
  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;
  paramData: any = {}; taskList: any = []; leadType = ''; itemLength = null;
  constructor(
    public activatedRoute: ActivatedRoute,
    public router: Router,
    public smartbuddyService: SmartbuddyService,
  ) { }

  ngOnInit() {
    let that = this;
    this.activatedRoute.params.subscribe(params => {
      this.leadType = params.type
      this.paramData = JSON.parse(params.data);
      //console.log(this.paramData, this.leadType);
      this.getTaskList();
    });
  }

  getTaskList() {
    let that = this;
    this.taskList = [];
    let param = {
      id: this.paramData.id
    }

    this.smartbuddyService.getLeads(param, function (response, status) {
      //console.log(response);
      if (status == 1) {
        let tmpArr = [];
        if (that.leadType == 'LeadsDomestic') {
          for (let i in response) {
            if (response[i]['lead_type'] == 1) {
              tmpArr.push(response[i]);
            }
          }
        } else if (that.leadType == 'LeadsOverseas') {
          for (let i in response) {
            if (response[i]['lead_type'] == 2) {
              tmpArr.push(response[i]);
            }
          }
        } else if (that.leadType == 'FollowUp') {
          for (let i in response) {
            if (response[i]['leadStatus'] == 2) {
              tmpArr.push(response[i]);
            }
          }
        } else {
          for (let i in response) {
            if (response[i]['leadStatus'] == 3) {
              tmpArr.push(response[i]);
            }
          }
        }
        that.taskList = tmpArr;
      } else {
        that.taskList = [];
      }
      //console.log(that.taskList);
      that.itemLength = that.taskList.length;
    });
  }

  doInfinite(infiniteScroll) {
    let that = this;
    let start = this.itemLength;

    let param = {
      id: this.paramData.id,
      start: start,
      loaderHide: true
    };

    this.smartbuddyService.getLeads(param, function (response, status) {
      if (response.length > 0) {
        if (that.leadType == 'LeadsDomestic') {
          for (let i in response) {
            if (response[i]['lead_type'] == 1) {
              that.taskList.push(response[i]);
            }
          }
        } else if (that.leadType == 'LeadsOverseas') {
          for (let i in response) {
            if (response[i]['lead_type'] == 2) {
              that.taskList.push(response[i]);
            }
          }
        } else if (that.leadType == 'FollowUp') {
          for (let i in response) {
            if (response[i]['leadStatus'] == 2) {
              that.taskList.push(response[i]);
            }
          }
        } else {
          for (let i in response) {
            if (response[i]['leadStatus'] == 3) {
              that.taskList.push(response[i]);
            }
          }
        }
        that.itemLength = that.taskList.length;
        infiniteScroll.target.complete();
      } else {
        infiniteScroll.target.complete();
        infiniteScroll.disabled = true;
      }
    });
  }

  openClickedButton(item) {
    this.router.navigate(['/wicereportdetail', item]);
  }
}
