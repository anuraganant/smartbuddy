import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SbWicereportlistPage } from './sb-wicereportlist.page';

describe('SbWicereportlistPage', () => {
  let component: SbWicereportlistPage;
  let fixture: ComponentFixture<SbWicereportlistPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SbWicereportlistPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SbWicereportlistPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
