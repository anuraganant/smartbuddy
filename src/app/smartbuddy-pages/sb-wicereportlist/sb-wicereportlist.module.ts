import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';
import { SharedModule } from '../../../app/shared.module';
import { SbWicereportlistPage } from './sb-wicereportlist.page';

const routes: Routes = [
  {
    path: '',
    component: SbWicereportlistPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [SbWicereportlistPage]
})
export class SbWicereportlistPageModule {}
