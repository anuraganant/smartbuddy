import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  {
    path: 'home',
    loadChildren: './home/home.module#HomePageModule'
  },
  {
    path: 'features',
    loadChildren: './smartbuddy-pages/sb-features/sb-features.module#SbFeaturesPageModule'
  },
  {
    path: 'livepopup',
    loadChildren: './smartbuddy-pages/sb-livepopup/sb-livepopup.module#SbLivepopupPageModule'
  },
  {
    path: 'wicereportlist',
    loadChildren: './smartbuddy-pages/sb-wicereportlist/sb-wicereportlist.module#SbWicereportlistPageModule'
  },
  {
    path: 'wicereportdetail',
    loadChildren: './smartbuddy-pages/sb-wicereportdetail/sb-wicereportdetail.module#SbWicereportdetailPageModule'
  },
  {
    path: 'assignleadlist',
    loadChildren: './smartbuddy-pages/sb-assignleadlist/sb-assignleadlist.module#SbAssignleadlistPageModule'
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
