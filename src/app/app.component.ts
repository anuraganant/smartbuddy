import { Component } from '@angular/core';
import { Platform, AlertController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Router } from '@angular/router';
import { AlertUtils } from '../providers/alertutils';
import { UtilService } from '../app/api/util.service';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  splashImage = true; 
  constructor(
    private platform: Platform,
    private alertUtils:AlertUtils,
    private splashScreen: SplashScreen,
    private screenOrientation: ScreenOrientation,
    private androidPermissions: AndroidPermissions,
    private statusBar: StatusBar,
    private router: Router,
    private utilService:UtilService,
    private alertController: AlertController
  ) {
    this.getRequest();
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.splashImage = false;
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      this.screenOrientation.lock('portrait');
      this.router.navigate(['/home']);
    });

    this.platform.backButton.subscribe(async (event) => {
      this.utilService.hideSpinner();
      let url = this.router.url.split(";");
      let path  = url[0];
      
      if (path == '/features' || path == '/home') {
        navigator['app'].exitApp();
      }
    });
  }

  getRequest(){
    this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE]);

    this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE).then(
      result => {
      },
      err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE)
    );
  }
}
