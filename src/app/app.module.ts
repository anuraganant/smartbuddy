import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { HttpModule } from '@angular/http';
import { IonicStorageModule } from '@ionic/storage';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { SharedModule } from './shared.module';
import { NgxSpinnerModule } from 'ngx-spinner';

import { TimeUtils } from '../providers/timeutils';
import { AlertUtils } from '../providers/alertutils';
import { UserData } from '../providers/userdata';
import { SharedService } from '../services/shared.service';
import { JModel } from '../services/JModel';
import { ConstantService } from '../services/constant.service';
import {
  LoginForm, WiceForm,
} from '../services/form-constants.model';
import { NativeGeocoder } from '@ionic-native/native-geocoder/ngx';
import { SpeechRecognition } from '@ionic-native/speech-recognition/ngx';
import { Downloader } from '@ionic-native/downloader/ngx';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { DocumentViewer } from '@ionic-native/document-viewer/ngx';
import { FileOpener } from '@ionic-native/file-opener/ngx';

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule,
    SharedModule,
    HttpModule,
    NgxSpinnerModule, 
    IonicStorageModule.forRoot(),
    IonicModule.forRoot(),
    AppRoutingModule
  ],
  providers: [
    StatusBar,
    SplashScreen, NativeGeocoder, SpeechRecognition, Downloader, ScreenOrientation,
    AndroidPermissions, DocumentViewer, FileOpener,
    SharedService, ConstantService, JModel, UserData, TimeUtils, AlertUtils,
    LoginForm, WiceForm,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
