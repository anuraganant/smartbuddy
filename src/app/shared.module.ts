
import { CommonModule } from '@angular/common';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { HsFooterComponent } from './components/hs-footer/hs-footer.component';
import { HsIconComponent } from './components/hs-icon/hs-icon.component';
import { HsErrorComponent } from './components/hs-error/hs-error.component';
import { HsMenuComponent } from './components/hs-menu/hs-menu.component';
import { HsCustomHeaderComponent } from './components/hs-custom-header/hs-custom-header.component';

@NgModule({
    imports: [CommonModule],

    schemas: [CUSTOM_ELEMENTS_SCHEMA],

    declarations: [
        HsFooterComponent, HsIconComponent, HsErrorComponent, HsMenuComponent, HsCustomHeaderComponent
    ],

    exports: [
        FormsModule, HsFooterComponent, HsIconComponent, HsErrorComponent, HsMenuComponent, HsCustomHeaderComponent
    ]
})
export class SharedModule { }