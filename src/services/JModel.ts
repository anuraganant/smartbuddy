export class JModel{
  data: any;
  

  constructor() {
    this.data = {
      aboutus:[],
      contactus:[],
      appData:{},
      userData:{},
      cartList:[],
      colorList:[],
      timeInfo:"",
      notification:[],
      category:[],
      facilitatorList:[],
      courseList:[],
      areaList:[],
      announcement:[],
      isNotification:"false"
    };    
  }

  get(key:any){
    return  this.data[key] || {};
  }

  getAll(){
    return  this.data;  
  }

  show(){
    //console.log(this.data);
  }
  showLog(s:any){
    //console.log(s, this.data);
  }
  
  set(key:any, value:any) {
    this.data[key] = value;
  }
}
