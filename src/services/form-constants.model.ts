
export class LoginForm {
    mobile: string
    password: string
}

export class WiceForm {
    contact_person: string
    contact_person_no: string
    newContactName: string
    newContactNo: string
    company_name: string
    comp_phone: string
    comp_city: string
    comp_state: string
    revelant: boolean
    changeName: boolean
    changeNo: boolean
    comment: string
    discussion: string
    nextFollowupDate: string
    nextFollowupTime: string
}