import { Component, Injectable, Input, Output, EventEmitter } from '@angular/core'

@Injectable()
export class SharedService {
  @Output() menu: EventEmitter<any> = new EventEmitter();
  @Output() annoucement: EventEmitter<any> = new EventEmitter();
  @Output() appdata: EventEmitter<any> = new EventEmitter();
  @Output() userdata: EventEmitter<any> = new EventEmitter();


  constructor() { }

  updateAppData(appdata: any) {
    this.appdata.emit(appdata);
  }

  getAppData() {
    return this.appdata;
  }

  updateUserData(userdata: any) {
    this.userdata.emit(userdata);
  }

  getUserData() {
    return this.userdata;
  }

  updateMenu(menu: any) {
    this.menu.emit(menu);
  }

  getMenu() {
    return this.menu;
  }

  updateAnnouncement(annoucement: any) {
    this.annoucement.emit(annoucement);
  }

  getAnnouncement() {
    return this.annoucement;
  }
} 