import { Injectable } from '@angular/core';
import { Platform } from '@ionic/angular';
import { SharedService } from '../services/shared.service';

@Injectable({
  providedIn: 'root'
})
export class ConstantService {
  server: any = {};
  url: any = {};
  platform: any = '';
  message: any = {};
  userData: any = {};
  constructor(
    public platf: Platform,
    public sharedService: SharedService,
    ) {
    this.init();
  }

  init() {
    this.url = this.getUrl();
    this.message = this.getMessage();
    this.platform = this.getPlatform();
  }


  getUrl() {
    var extn = '.php';
    return {
      API_PATH: 'http://122.160.84.220:8080/hartrodt/services/',

      DEFAULT_EVENT_IMAGE: 'assets/images/eventdefault.jpg',
      DEFAULT_GALLERY_IMAGE: 'assets/images/gallery.jpg',
     
      LOGIN: 'loginServices' + extn,
    }
  }

  getPlatform() {
    let platType = 'android';
    if (this.platf.is('ios')) {
      platType = 'ios'
    }
    return platType;
  }

  getMessage() {
    return {
      login: 'Successfully Loggedin',
      registration: '',
      post: 'Successfully Added',
      update: 'Successfully Updated',
      delete: 'Successfully Deleted',
      error: 'No record found !',
      mandatory: 'Please fill all the mandatory fields!',
      checkbox: 'Please select check box to proceed!',
      server: 'Unable to process request..',
      loginfail: 'Incorrect Mobile No or Password!',
    }
  }
}
