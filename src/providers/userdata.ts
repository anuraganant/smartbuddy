import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

@Injectable()
export class UserData {
    _favorites: string[] = [];
    COMPANY_NAME = "Hari Suvidha";
    HAS_LOGGED_IN = 'hasLoggedIn';
    HAS_SEEN_TUTORIAL = 'hasSeenTutorial';
    ALERT = null;
    cat_id = 1;
    par_id = 1;
    toastId = null;

    constructor(
        public storage: Storage
    ) { }

    getAppName() {
        return this.COMPANY_NAME;
    }

    hasFavorite(sessionName: string) {
        return (this._favorites.indexOf(sessionName) > -1);
    };

    addFavorite(sessionName: string) {
        this._favorites.push(sessionName);
    };

    removeFavorite(sessionName: string) {
        let index = this._favorites.indexOf(sessionName);
        if (index > -1) {
            this._favorites.splice(index, 1);
        }
    };

    login(username: string, user: any, callback: (doc: any) => void) {
        this.storage.set(this.HAS_LOGGED_IN, true);
        this.setUsername(username);
        this.setUserDetails(user);
        this.setUserImage(user["imgName"]);
        //this.events.publish('user:login');
        callback(user);
    };

    setUsername(username: string) {
        this.storage.set('username', username);
    };

    getUsername() {
        return this.storage.get('username').then((value) => {
            return value;
        });
    };

    setUserImage(userImage: string) {
        this.storage.set('username', userImage);
    };

    getUserImage(callback: any) {
        return this.storage.get('username').then((value) => {
            callback(value);
        });
    };

    setUserDetails(user: any) {
        user["imgName"] = this.getImageName(user.user_name);
        this.storage.set('user_details', user);
    };

    getUserDetails(callback: any) {
        return this.storage.get('user_details').then((value) => {
            callback(value);
        });
    };

    setAnnouncements(user: any) {
        this.storage.set('announcements', user);
    };

    getAnnouncements(callback: any) {
        return this.storage.get('announcements').then((value) => {
            callback(value);
        });
    };

    setEventDetails(event: any) {
        this.storage.set('evn_details', event);
    };

    getEventDetails(callback: any) {
        return this.storage.get('evn_details').then((value) => {
            callback(value);
        });
    };

    setCreditPoints(points: any) {
        this.storage.set('points', points);
    };

    getCreditPoints(callback: any) {
        return this.storage.get('points').then((value) => {
            callback(value);
        });
    };

    logout(callback) {
        this.storage.remove(this.HAS_LOGGED_IN).then(() => {
            this.storage.remove('username').then(() => {
                this.storage.remove('user:login').then(() => {
                    this.storage.remove('user_details').then(() => {
                        if (typeof (callback) == 'function') {
                            callback();
                        }
                    })
                })
            })
        })
    };

    setSession(key: any, value: any) {
        this.storage.set('hst_url_' + key, value);
    };

    removeAllCached() {
        var list = [];
        this.storage.forEach((value, key, index) => {
            if (key.indexOf('hst_url_') >= 0) {
                list.push(key);
            }
        });

        for (var i in list) {
            this.storage.remove('' + list[i]);
        }
    }

    getSession(key: any, callback: any) {
        //this.removeAllCached();
        return this.storage.get('hst_url_' + key).then((value) => {
            if (value) {
                callback(value);
            } else {
                callback(null);
            }

        });
    };

    hasLoggedIn() {
        return this.storage.get(this.HAS_LOGGED_IN).then((value) => {
            return value === true;
        });
    };

    checkHasSeenTutorial() {
        return this.storage.get(this.HAS_SEEN_TUTORIAL).then((value) => {
            return value;
        })
    };

    replaceAll(data: any, search: any, replacement: any) {
        return data.replace(new RegExp(search, 'g'), replacement);
    };

    removeWhite(str: any) {
        str = str.replace(/  +/g, ' ');
        return str.trim();
    }

    getImageName(name: any) {
        if (name && name != '') {
            name = name.toUpperCase();
            var matches = name.match(/\b(\w)/g);
            var len = matches.length, start = 1;
            if (len == start) {
                return name.substring(0, 2);
            } else {
                return matches[0] + matches[len - start];
            }
        } else {
            name = 'IYF';
        }

    }
}

