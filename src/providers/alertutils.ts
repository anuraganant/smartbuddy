import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { AlertController, LoadingController, ActionSheetController, Platform, Events } from '@ionic/angular';
// import { UserData } from './user-data';
import { JModel } from '../services/JModel';
import { ToastController } from '@ionic/angular';
import { map } from 'rxjs/operators';
import { ConstantService } from '../services/constant.service';

@Injectable()
export class AlertUtils {

  JQUERY: any;
  PATH = '';
  PATH_IMAGE = '';
  ALERT = null;
  TOAST = null;
  LOADER = null;
  MOBILE = null;
  FEESTATUS = null;
  REGLIMIT = null;
  NETWORKTYPE = null;
  ERROR_ALERT = null;
  CACHED = null;
  disconnect: any;
  username = '';
  items = [];
  isApp = null;
  platformType = null;
  facilitatorList: any;
  courseList: any;
  areaList: any;

  constructor(
    public actionSheetController: ActionSheetController,
    public cs: ConstantService,
    public jModel: JModel,
    private alertController: AlertController,
    public loadingCtrl: LoadingController,
    public http: Http,
    // public userData: UserData,
    public events: Events,
    public platform: Platform,
    public toastController: ToastController,
  ) {
    this.JQUERY = window["$"];
    this.LOADER = null;
    this.isApp = false;

    this.platformType = "";
    this.platform.ready().then(() => {
      if (this.platform.is('android') || this.platform.is('ios')) {
        this.isApp = true;
      } else {
        this.isApp = false;
      }
    });
    this.CACHED = jModel.get('appData')['cached'] || "";
  }

  checkVersion(version: any, verArr: any) {
    let verFlag = false;
    for (let i in verArr) {
      if (verArr[i] == version) {
        verFlag = true;
        break;
      }
    }
    return verFlag;
  }

  inArray(item: any, ItemList: any) {
    return this.JQUERY.inArray(item, ItemList)
  }

  getPlatformName() {
    let platform;
    if (this.platform.is('android')) {
      platform = 'android';
    } else if (this.platform.is('ios')) {
      platform = 'ios';
    }
    return platform;
  }

  setNetworkType(nwType: any) {
    this.NETWORKTYPE = nwType;
  };

  getNetworkType() {
    return this.NETWORKTYPE;
  };

  async alertToast(message) {
    const toastSuccess = await this.toastController.create({
      message: message,
      position: 'top',
      duration: 2000,
    });
    toastSuccess.present();
  }

  async successToast(message) {
    const toastSuccess = await this.toastController.create({
      message: message,
      position: 'top',
      duration: 2000,
      cssClass: 'successToastcss',
    });
    toastSuccess.present();
  }


  async errorToast(message) {
    const toastError = await this.toastController.create({
      message: message,
      position: 'top',
      duration: 2000,
      cssClass: 'errorToastcss',
    });
    toastError.present();
  }

  async OnError() {
    let alert = await this.alertController.create({
      header: '<h4>SORRY FOR THE DELAY<h4>',
      message: 'WE HAD SOME TROUBLE CONNECTING, BUT SHOULD HAVE YOU MOVING SHORTLY.',
      buttons: [
        {
          text: 'Ok',
          cssClass: 'alertConfirm',
          handler: () => {
            location.reload();
          }
        }
      ]
    });
    await alert.present();
  }

  async onConnect() {
    if (this.ALERT != null) {
      this.ALERT.dismiss();
      this.ALERT = null;
    }
  }

  async onDisconnect() {
    if (this.ALERT == null) {
      this.presentLoadingWithOptions()
      this.disconnect = null;
    }
  }

  async presentLoadingWithOptions() {
    const loading = await this.loadingCtrl.create({
      message: 'We can not reach your network right now, Please check your internet connection.',
      translucent: true,
    });
    return await loading.present();
  }


  removeWhite(str: any) {
    str = str.replace(/  +/g, ' ');
    return str.trim();
  }

  removeAarry(arr: any, what: any) {
    var tm = [];
    for (var i in arr) {
      if (arr[i] !== what) {
        tm.push(arr[i]);
      }
    }
    return tm;
  }


  console(...args: any[]) {
    let flag = false;
    if (flag) {
    }
  }

  sortByKey(array, key) {
    return array.sort(function (a, b) {
      var x = a[key]; var y = b[key];
      return ((x < y) ? -1 : ((x > y) ? 1 : 0));
    });
  }

  closeBack() {
    if (this.LOADER != null) {
      this.LOADER.dismiss();
      this.LOADER = null;
    }
  }


  async showConfirm(title: any, message: any, okayCallback: any, cancelCallback: any) {
    let confirm = await this.alertController.create({
      header: title,
      message: message,
      buttons: [
        {
          text: 'Disagree',
          cssClass: 'alertCancel',
          handler: () => {
            if (typeof (cancelCallback) == "function")
              cancelCallback();

          }
        },
        {
          text: 'Agree',
          cssClass: 'alertConfirm',
          handler: () => {
            if (typeof (cancelCallback) == "function")
              okayCallback();
          }
        }
      ]
    });
    await confirm.present();
  }

  serialize(obj: any) {
    var str = "";
    for (var key in obj) {
      if (str != "") {
        str += "&";
      }
      str += key + "=" + encodeURIComponent(obj[key]);
    }
    return str;
  }

  async successAlert(data: any) {
    const alert = await this.alertController.create({
      header: 'Success!',
      message: data,
      buttons: [
        {
          text: 'Ok',
          cssClass: 'alertConfirm',
        }
      ]
    });

    await alert.present();
  }

  async errorAlert(data: any) {
    const alert = await this.alertController.create({
      header: 'Information!',
      message: data,
      buttons: [
        {
          text: 'Ok',
          cssClass: 'alertConfirm',
        }
      ]
    });

    await alert.present();
  }

  async infoAlert(title: any, data: any) {
    let alert = await this.alertController.create({
      header: title,
      message: data,
      buttons: [
        {
          text: 'Ok',
          cssClass: 'alertConfirm',
        }
      ]
    });
    await alert.present();
  }

  async getDeleteConfirmation(title: any, message: any, callBack: any) {
    let confirm = await this.alertController.create({
      header: title,
      message: message,
      buttons: [
        {
          text: 'Cancel',
          cssClass: 'alertCancel',
          handler: () => {
            callBack(false);
          }
        },
        {
          text: 'Delete',
          cssClass: 'alertConfirm',
          handler: () => {
            callBack(true);
          }
        }
      ]
    });
    await confirm.present();
  }

  async getEditConfirmation(title: any, message: any, callBack: any) {
    let confirm2 = await this.alertController.create({
      header: title,
      message: message,
      buttons: [
        {
          text: 'Cancel',
          cssClass: 'alertCancel',
          handler: () => {
            callBack(false);
          }
        },
        {
          text: 'Edit',
          cssClass: 'alertConfirm',
          handler: () => {
            callBack(true);
          }
        }
      ]
    });
    await confirm2.present();
  }

  async activatedeactivateUser(title: any, message: any, callBack: any) {
    let confirm3 = await this.alertController.create({
      header: title,
      message: message,
      buttons: [
        {
          text: 'Cancel',
          cssClass: 'alertCancel',
          handler: () => {
            callBack(false);
          }
        },
        {
          text: 'Confirm',
          cssClass: 'alertConfirm',
          handler: () => {
            callBack(true);
          }
        }
      ]
    });
    await confirm3.present();
  }

  async selectImageOption(callBack: any) {
    let actionSheet = await this.actionSheetController.create({
      header: 'Please select any option to continue',
      cssClass: 'action-sheets-basic-page',
      buttons: [
        {
          text: 'Use Camera',
          cssClass: 'sheetConfirm',
          handler: () => {
            if (typeof (callBack) == "function")
              callBack('camera');
          }
        }, {
          text: 'Use Gallery',
          cssClass: 'sheetConfirm',
          handler: () => {
            if (typeof (callBack) == "function")
              callBack('gallery');
          }
        }, {
          text: 'Cancel',
          cssClass: 'sheetCancel',
          role: 'cancel',
          handler: () => {
            //console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
  }

  async changeImageConfirmation(callBack: any) {
    let changeImage = await this.alertController.create({
      header: 'Upload Image!',
      message: 'This will replace the existing image.',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'alertCancel',
          handler: (blah) => {
            if (typeof (callBack) == "function")
              callBack('false');
          }
        }, {
          text: 'Ok',
          cssClass: 'alertConfirm',
          handler: () => {
            if (typeof (callBack) == "function")
              callBack('true');
          }
        }
      ]
    });
    changeImage.present();
  }

  async getEventOption(callBack: any) {
    const actionSheet = await this.actionSheetController.create({
      header: 'Select Option',
      buttons: [{
        text: 'Add Event',
        cssClass: 'sheetConfirm',
        handler: () => {
          if (typeof (callBack) == "function")
            callBack('add');
        }
      }, {
        text: 'View Event',
        cssClass: 'sheetConfirm',
        handler: () => {
          if (typeof (callBack) == "function")
            callBack('view');
        }
      }, {
        text: 'Cancel',
        cssClass: 'sheetCancel',
        handler: () => {
          if (typeof (callBack) == "function")
            callBack('cancel');
        }
      }]
    });
    await actionSheet.present();

  }

  async getCateoryOption(callBack: any) {
    const actionSheet = await this.actionSheetController.create({
      header: 'Select Option',
      buttons: [{
        text: 'Edit',
        icon: 'checkbox-outline',
        cssClass: 'sheetConfirm',
        handler: () => {
          if (typeof (callBack) == "function")
            callBack('Edit');
        }
      }, {
        text: 'Delete',
        icon: 'trash',
        cssClass: 'sheetConfirm',
        handler: () => {
          if (typeof (callBack) == "function")
            callBack('Delete');
        }
      }, {
        text: 'Cancel',
        cssClass: 'sheetCancel',
        handler: () => {
          if (typeof (callBack) == "function")
            callBack('cancel');
        }
      }]
    });
    await actionSheet.present();

  }


  getCapitalizeFirstLetter(str) {
    return str.replace(
      /\w\S*/g,
      function (txt) {
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
      }
    );
  }
}
