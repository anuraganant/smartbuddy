import { Injectable } from '@angular/core';
@Injectable()

export class TimeUtils {

  constructor(

  ) {

  }

  formatDate(d) {
    var monthNames = [
      "Jan", "Feb", "Mar",
      "April", "May", "June", "July",
      "Aug", "Sep", "Oct",
      "Nov", "Dec"
    ];
    var date = new Date(d);

    var day = date.getDate();
    var monthIndex = date.getMonth();
    var year = date.getFullYear();

    return day + '-' + monthNames[monthIndex] + '-' + year;
  }

  setDateFormatForServe(date) {
    return date['year']['value'] + '-' + date['month']['value'] + '-' + date['day']['value'];
  }

  setReportDateForServe(date) {
    return date['year']['text'] + '-' + date['month']['text'] + '-' + date['day']['text'];
  }

  setDateFormatForUI(date) {
    //console.log(date);
    let d = date.split('-');
    //console.log(d);
    return {
      day: {
        columnIndex: 0,
        text: d[2],
        value: parseInt(d[2])
      },
      month: {
        columnIndex: 1,
        text: d[1],
        value: parseInt(d[1])
      },
      year: {
        columnIndex: 2,
        text: d[0],
        value: parseInt(d[0])
      }
    };
  }

  setEventDateFormatForServe(date) {
    return date['year']['text'] + '-' + date['month']['value'] + '-' + date['day']['text'] + ' ' + date['hour']['text'] + ':' + date['minute']['text'] + ' ' + date['ampm']['text'];
  }

  setEventDateFormatForEvent(date) {
    return date['year']['text'] + ', ' + date['month']['value'] + ', ' + date['day']['text'] + ', ' + date['hour']['text'] + ', ' + date['minute']['text'] + ', ' + '00';
  }

  setTimeFormatForServe(time) {
    //console.log(time);
    return time['hour']['text'] + ':' + time['minute']['text'];
  }

  setTimeFormatForServeWithAMPM(time) {
    //console.log(time);
    return time['hour']['text'] + ':' + time['minute']['text'] + ' ' + time['ampm']['text'];
  }

  setEventDateFormatForUI(date) {
    // let a:any, d:any, t:any;
    let a = date.split(" ");

    let d = a[0];
    let t = a[1];
    let newDate = d.split("-");
    let newTime = t.split(":");
    //console.log(newDate, newTime);

    return {
      day: {
        columnIndex: 0,
        text: newDate[2],
        value: parseInt(newDate[2])
      },
      month: {
        columnIndex: 1,
        text: newDate[1],
        value: parseInt(newDate[1])
      },
      year: {
        columnIndex: 2,
        text: newDate[0],
        value: parseInt(newDate[0])
      },
      hour: {
        columnIndex: 3,
        text: newTime[0],
        value: parseInt(newTime[0])
      },
      minute: {
        columnIndex: 4,
        text: newTime[1],
        value: parseInt(newTime[1])
      },
      ampm: {
        columnIndex: 5,
        text: this.getAMPM(newTime[0]),
        value: this.getAMPM(newTime[0])
      }
    };
  }

  getDayName(date){
    let currentdate = new Date(date);
    let weekdays = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
    let dat = weekdays[currentdate.getDay()];
    return dat;
  }

  getAMPM(hours: any) {
    let ampm = hours >= 12 ? 'PM' : 'AM';
    return ampm;
  }

  setEventDateText(date) {
    // let a:any, d:any, t:any;
    let a = date.split(" ");

    let d = a[0];
    let t = a[1];
    let newDate = d.split("-");
    let newTime = t.split(":");
    //console.log(newDate, newTime);
    let arr1 = [newDate[2], newDate[1], newDate[0]];
    let arr2 = [newTime[0], newTime[1]];
    let finalFormat = arr1.join("-") + " " + arr2.join(":") + this.formatAMPM(newTime[0], newTime[1]);

    return finalFormat
  }


  getTotalMinutes(hours: any, minutes: any) {
    //console.log(hours, minutes);
    hours = parseInt(hours);
    minutes = parseInt(minutes);
    let min = hours * 60;
    let totalMins = min + minutes;
    return totalMins;
  }

  getTimeFromInt(data) {
    let minutes, hours, hr;
    data = parseInt(data);
    hours = Math.floor(data / 60);
    minutes = data % 60;
    hr = hours < 10 ? '0' + hours : hours;
    minutes = minutes < 10 ? '0' + minutes : minutes;
    let strTime = hr + ':' + minutes;
    return strTime;
  }

  getTimeFromString(data) {
    let minutes, hours, hr;
    data = parseInt(data);
    hours = Math.floor(data / 60);
    minutes = data % 60;
    hr = hours < 10 ? '0' + hours : hours + '';
    //minutes = minutes < 10 ? '0' + minutes  : minutes+'' ;

    //let strTime = hr + ':' + minutes;
    return {
      hour: {
        columnIndex: 0,
        text: hr,
        value: parseInt(hr)
      },
      minute: {
        columnIndex: 1,
        text: minutes,
        value: parseInt(minutes)
      }
    };
  }

  formatTime(hours: any, minutes: any) {
    let strTime;
    hours = parseInt(hours);
    minutes = parseInt(minutes);
    if (hours > 0) {
      hours = hours % 24;
      hours = hours ? hours : 24;
      minutes = minutes < 10 ? '0' + minutes : minutes;
      strTime = hours + ':' + minutes + " hr";
    } else {
      minutes = minutes < 10 ? '0' + minutes + " min" : minutes + " min";
      strTime = minutes;
    }
    return strTime;
  }

  pointFormatTime(hours: any, minutes: any) {
    let strTime;
    hours = parseInt(hours);
    minutes = parseInt(minutes);
    if (hours > 0) {
      hours = hours % 24;
      hours = hours ? hours : 24;
      minutes = minutes < 10 ? '0' + minutes : minutes;
      strTime = hours + ':' + minutes;
    } else {
      minutes = minutes < 10 ? '0' + minutes : minutes;
      strTime = minutes;
    }
    return strTime;
  }



  getformatTime(hours: any, minutes: any) {
    let strTime;
    hours = parseInt(hours);
    minutes = parseInt(minutes);
    if (hours > 0) {
      hours = hours % 24;
      hours = hours ? hours : 24;
      minutes = minutes < 10 ? '0' + minutes : minutes;
      strTime = hours + ':' + minutes + " hr";
    } else {
      minutes = minutes < 10 ? '0' + minutes + " min" : minutes + " min";
      strTime = minutes;
    }
    return {
      hour: {
        columnIndex: 0,
        text: hours,
        value: parseInt(hours)
      },
      minute: {
        columnIndex: 1,
        text: minutes,
        value: parseInt(minutes)
      }
    };
  }

  formatMinutes(minutes: any) {
    minutes = parseInt(minutes);
    let strTime = minutes + " min";
    return strTime;
  }

  dayDiff(date, diff) {
    let d, month, day;
    d = new Date(date);
    d.setDate(d.getDate() + (diff));
    day = d.getUTCDate();
    month = d.getUTCMonth() + 1;
    month = (month < 10) ? '0' + month : month;
    day = (day < 10) ? '0' + day : day;
    return d.getUTCFullYear() + '-' + month + '-' + day;
  }

  formatAMPM(hours: any, minutes: any) {
    //console.log(hours, minutes);
    hours = parseInt(hours);
    minutes = parseInt(minutes);
    let ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0' + minutes : minutes;
    let strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
  }
}
